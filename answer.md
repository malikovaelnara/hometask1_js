var, let and const are used for decleration of variables.
But there are many differences between them.
Var is called function scope. It means that if the variable is local variable which declerate by var keyword then it can just be inside the function block.If we use var then we can redeclerate and reassign the variable.
Let and const are called block scope.It means that if the variable is local variable which declerate by let or const keyword then it can be inside any kind of blocks including function block.
Variable can be reassigned but can't be redeclerated which derlerate by let keyword.
Variable can be neither reassigned nor redeclerated which derlerate by const keyword.